# What is Tolerance
An application which allows you to setup tolerances for temperature sensors and display them on a line chart

![Tolerance](main.png)

# Setup and installation
```
npm install
npm run prod
```

# Technical overview
## MEAN Stack
This project uses the MEAN stack:

#### [**M**ongoose](http://www.mongoosejs.com)/[MongoDB](https://www.mongodb.com)
`MongoDB` was chosen as the database, since it natively supports `JSON` serialization, and it plays very well as part of a `NodeJS` stack (supports streams)

#### [**E**xpress](http://expressjs.com)
`Express` was chosen as the backend framework due to its simplicity, adding a very thin layer between the frontend and the database

#### [**A**ngular 2+](https://angular.io)
`Angular 2` was chosen since it allows for building a very performant app, with such features as: Code splitting and its progressive web app capabilities - allowing for a future-proof code base

#### [**N**ode.js](https://nodejs.org)
`NodeJS` was chosen as the runtime environment due to its performance characteristics 

## Additional Technologies used

##### [Chart.js](http://www.chartjs.org/)
Simple, clean and engaging `HTML5` based `JavaScript` charts.  `Chart.js` supports a huge feature set and is well-adopted within the developer community.  Although it has some limitation (canvas-based and controlling the display of tooltips is fairly limited), it is the better choice for this project. 

#### [ES6](http://es6-features.org/#Constants)
`ES6` added a lot of features, which significantly speeds up the development process.  Additionally, this codebase makes extensive use of `Await/Async` to simplify code and avoid the dreaded [callback hell](http://callbackhell.com/)

#### [@ngrx](https://github.com/ngrx/store)
`@ngrx` is RxJS powered state management for Angular applications, inspired by Redux.  Using this in the project significantly reduces the complexity with state management and allows for automatic data-binding to the `DOM`

#### [Typescript](https://www.typescriptlang.org/)
`Typescript` is a superset of JavaScript which provides optional static typing, classes and interfaces.  These features enable IDEs to provide a richer environment for spotting common errors - which significantly reduces the feedback-loop - increasing productivity

#### [Webpack](https://webpack.js.org/)
Perhaps the main reason `Webpack` was considered for this project was due to its ability to split the dependency tree into chucks, and only load them when needed.  Additionally, `Webpack` has a plethora of plugins and modules, which can be easily added to improve different aspects of the build

#### [Bootstrap](http://getbootstrap.com/)
`Bootstrap` was used in this project due to its simple configuration.  Additionally, since `bootstrap` is a widely adopted CSS framework, it means that any developer picking up this project in the future should be able to pick up the project very easily
