import {
  Component,
  OnInit,
  ViewChild,
  SimpleChanges,
  ElementRef
} from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Rx";

import Chart from "chart.js";
import { BaseChartDirective } from "ng2-charts/ng2-charts";

import * as ToleranceActions from "../store/tolerance/tolerance.action";
import * as ToleranceReducer from "../store/tolerance/tolerance.reducers";
import { ToleranceState } from "../store/tolerance/tolerance.state";
import Tolerance from "../models/tolerance.model";
import { AppState } from "../store/state";

@Component({
  selector: "app-tolerance-chart",
  templateUrl: "./tolerance-chart.component.html",
  styleUrls: ["./tolerance-chart.component.scss"]
})
export class ToleranceChartComponent implements OnInit {
  // lineChart
  @ViewChild(BaseChartDirective) Chart: BaseChartDirective;

  lineChartData: Array<any> = [];
  lineChartLabels: Array<any> = [];
  lineChartOptions: any = {
    responsive: true,
    tooltips: {
      mode: "point"
    }
  };

  lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "#ccffcc",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)",
      fill: false
    },
    {
      // dark grey
      backgroundColor: "rgba(77,83,96,0.2)",
      borderColor: "rgba(77,83,96,1)",
      pointBackgroundColor: "cc33ff",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(77,83,96,1)",
      fill: false
    }
  ];
  lineChartLegend = true;
  lineChartType = "line";

  scheduleList$: Observable<any>;
  tolerance:Tolerance;

  constructor(public store: Store<AppState>) {}

  ngOnInit() {
    this.scheduleList$ = this.store.select("tolerance");

    this.scheduleList$.subscribe(data => {
        this.tolerance = data;
        this.lineChartData = [];
        this.lineChartLabels = [];
        this.Chart.ngOnChanges({} as SimpleChanges);

      if (data && data.lineChartData && data.lineChartLabels) {
        this.lineChartData = data.lineChartData;
        this.lineChartLabels = data.lineChartLabels;
        this.Chart.ngOnChanges({} as SimpleChanges);
      }
    });
  }
  // events
  chartClicked(e: any): void {
    console.log(e);
  }

  chartHovered(e: any): void {
    console.log(e);
  }
  onClick() {
    // this.lineChartData[0].data.push(Math.random() * 99);
  }
  randomize() {
    let _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {
        data: new Array(this.lineChartData[i].data.length),
        label: this.lineChartData[i].label
      };
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor(Math.random() * 100 + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }
}
