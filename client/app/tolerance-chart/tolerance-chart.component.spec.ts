import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToleranceChartComponent } from './tolerance-chart.component';

describe('ToleranceChartComponent', () => {
  let component: ToleranceChartComponent;
  let fixture: ComponentFixture<ToleranceChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToleranceChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToleranceChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
