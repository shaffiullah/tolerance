import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Rx";
import { ToleranceService } from "../services/tolerance.service";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";
import { ToastComponent } from "../shared/toast/toast.component";
import Tolerance from "../models/tolerance.model";
import { AppState } from "../store/state";
import * as ToleranceActions from "../store/tolerance/tolerance.action";

@Component({
  selector: "app-tolerance",
  templateUrl: "./tolerance.component.html",
  styleUrls: ["./tolerance.component.scss"]
})
export class ToleranceComponent implements OnInit {
  tolerances: Array<Tolerance> = [];
  isLoading = true;
  addToleranceForm: FormGroup;
  name = new FormControl("", Validators.required);
  constructor(
    private toleranceService: ToleranceService,
    private formBuilder: FormBuilder,
    private toast: ToastComponent,
    public store: Store<AppState>
  ) {}

  ngOnInit() {
    this.toleranceService.getTolerances().subscribe(
      data => {
        this.tolerances = data;
        if (this.tolerances.length) {
          this.store.dispatch({
            type: ToleranceActions.SELECT_TOLERANCE,
            payload: this.tolerances[0]
          });
        }
      },
      error => console.log(error),
      () => (this.isLoading = false)
    );
    this.addToleranceForm = this.formBuilder.group({
      name: this.name
    });
  }
  addTolerance() {
    this.toleranceService.addTolerance(this.addToleranceForm.value).subscribe(
      res => {
        const newTolerrance = res.json();
        this.tolerances.push(this.addToleranceForm.value);
        this.addToleranceForm.reset();
        this.toast.setMessage("item added successfully.", "success");
      },
      error => console.log(error)
    );
  }
  onClick(tolerance) {
    this.store.dispatch({
      type: ToleranceActions.SELECT_TOLERANCE,
      payload: tolerance
    });
  }
}
