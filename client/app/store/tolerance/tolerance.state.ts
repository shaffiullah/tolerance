import Tolerance from "../../models/tolerance.model";

export interface ToleranceState extends Tolerance {
  loading?: boolean;



  error?: boolean;
}

export const initializeToleranceState = function() {
  return {
    loading: false,

    editable: true,
    edited: false,
    editing: false,

    selected: false,
    refreshing: false,

    create: true,

    error: false
  };
};

export interface ToleranceListState {
  tolerances?: ToleranceState[];
  loading?: boolean;
  pending?: number;
}

export const intializeToleranceListState = function() {
  return {
    loading: false,
    pending: 0
  };
};
