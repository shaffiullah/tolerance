import Tolerance from "../../models/tolerance.model";
import * as ToleranceActions from "./tolerance.action";
export type Action = ToleranceActions.All;
const newState = (state, newData) => {
  return Object.assign({}, state, newData);
};
export function ToleranceReducer(
  state: Tolerance = Tolerance.generateMockTolerance(),
  action: Action
) {
  switch (action.type) {
    case ToleranceActions.ADD_SCHEDULE: {
      const tolerance = state;
      tolerance.schedule.push(action.payload);
      tolerance.lineChartData[0].data.push(action.payload.minTemp);
      tolerance.lineChartData[1].data.push(action.payload.maxTemp);
      tolerance.lineChartLabels.push(
        parseInt(action.payload.endTime.toString(), 10)
      );
      return newState(state, tolerance);
    }
    case ToleranceActions.REMOVE_SCHEDULE: {
      const tolerance = state;
      tolerance.schedule.splice(action.payload, 1);
      tolerance.lineChartData[0].data.splice(action.payload, 1);
      tolerance.lineChartData[1].data.splice(action.payload, 1);
      tolerance.lineChartLabels.splice(action.payload, 1);
      return newState(state, tolerance);
    }
    case ToleranceActions.SELECT_TOLERANCE: {
      console.log(action.payload);
      const tolerance = state;
      tolerance._id = action.payload._id;
      tolerance.name = action.payload.name;
      tolerance.schedule = action.payload.schedule;
      if (tolerance.schedule) {
        tolerance.lineChartData[0].data = [];
        tolerance.lineChartData[1].data = [];
        tolerance.lineChartLabels = [];
        tolerance.schedule.map(schedule => {
          tolerance.lineChartData[0].data.push(schedule.minTemp);
          tolerance.lineChartData[1].data.push(schedule.maxTemp);
          tolerance.lineChartLabels.push(
            parseInt(schedule.endTime.toString(), 10)
          );
        });
      }
      return newState(state, tolerance);
    }
    default:
      return state;
  }
}
