import { ToleranceState } from "./tolerance.state";
import Tolerance from "../../models/tolerance.model";
import { Action } from "@ngrx/store";
export interface Schedule {
  startTime: String;
  endTime: String;
  minTemp: Number;
  maxTemp: Number;
}
export const GET_TOLERANCE = "[Tolerance] GET_TOLERANCE";

export const GET_TOLERANCE_SUCCESS = "[Tolerance] GET_TOLERANCE_SUCCESS";

export const GET_TOLERANCE_ERROR = "[Tolerance] GET_TOLERANCE_ERROR";

export const GET_TOLERANCES = "[Tolerance] GET_TOLERANCES";

export const GET_TOLERANCES_SUCCESS = "[Tolerance] GET_TOLERANCES_SUCCESS";

export const GET_TOLERANCES_ERROR = "[Tolerance] GET_TOLERANCES_ERROR";

export const ADD_SCHEDULE = "[Schedule] ADD_SCHEDULE";
export const REMOVE_SCHEDULE = "[number] REMOVE_SCHEDULE";

export const SELECT_TOLERANCE = "[Tolerance] SELECT_TOLERANCE";

export class GetTolerances implements Action {
  readonly type = GET_TOLERANCES;
}

export class GetTolerancesSuccess implements Action {
  readonly type = GET_TOLERANCES_SUCCESS;

  constructor(public payload: ToleranceState[]) {}
}

export class GetTolerancesError implements Action {
  readonly type = GET_TOLERANCES_ERROR;
}

export class GetTolerance implements Action {
  readonly type = GET_TOLERANCE;

  constructor(payload: string) {}
}

export class GetToleranceSuccess implements Action {
  readonly type = GET_TOLERANCE_SUCCESS;

  constructor(public payload: Tolerance) {}
}

export class GetToleranceError implements Action {
  readonly type = GET_TOLERANCE_ERROR;
}
export class AddSchedule implements Action {
  readonly type = ADD_SCHEDULE;

  constructor(public payload: Schedule) {}
}
export class RemoveSchedule implements Action {
  readonly type = REMOVE_SCHEDULE;

  constructor(public payload: number) {}
}
export class SelectTolerance implements Action {
  readonly type = SELECT_TOLERANCE;

  constructor(public payload: Tolerance) {}
}
export type All =
  | GetTolerances
  | GetTolerancesSuccess
  | GetTolerancesError
  | GetTolerance
  | GetToleranceSuccess
  | GetToleranceError
  | AddSchedule
  | RemoveSchedule
  | SelectTolerance;
