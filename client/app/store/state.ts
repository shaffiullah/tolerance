import Tolerance from "../models/tolerance.model";

export interface AppState {
  tolerance: Tolerance;
}
