import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Rx";
import "rxjs/add/observable/forkJoin";
import { ToastComponent } from "../shared/toast/toast.component";
import { ToleranceService } from "../services/tolerance.service";
import * as ToleranceActions from "../store/tolerance/tolerance.action";
import * as ToleranceReducer from "../store/tolerance/tolerance.reducers";
import { ToleranceState } from "../store/tolerance/tolerance.state";
import Tolerance from "../models/tolerance.model";
import { AppState } from "../store/state";
export interface Schedule {
  startTime: String;
  endTime: String;
  minTemp: Number;
  maxTemp: Number;
}

@Component({
  selector: "app-tolerance-detail",
  templateUrl: "./tolerance-detail.component.html",
  styleUrls: ["./tolerance-detail.component.scss"]
})
export class ToleranceDetailComponent implements OnInit {
  addScheduleForm = this.fb.group({
    startTime: "",
    endTime: "",
    minTemp: "",
    maxTemp: ""
  });
  schedulesForm = this.fb.group({
    schedulesControls: this.fb.array([])
  });

  schedules: Schedule[];
  isLoading = false;
  isEditing = false;
  tolerance$: Observable<any>;
  tolerance: Tolerance;
  constructor(
    private fb: FormBuilder,
    private toleranceService: ToleranceService,
    public toast: ToastComponent,
    public store: Store<AppState>
  ) {}

  ngOnInit() {
    this.tolerance$ = this.store.select("tolerance");
    this.tolerance$.subscribe(data => {
      this.schedulesForm.controls["schedulesControls"] = this.fb.array([]);
      this.tolerance = data;
      if (this.tolerance.schedule) {
        this.tolerance.schedule.map(schedule => {
          console.log(schedule);

          const control = <FormArray>this.schedulesForm.get(
            "schedulesControls"
          );
          control.push(this.createSchedule(schedule));
        });
      }
    });
  }
  createSchedule(schedule) {
    return this.fb.group({
      startTime: parseInt(schedule.startTime, 10) || "",
      endTime: parseInt(schedule.endTime, 10) || "",
      minTemp: parseInt(schedule.minTemp, 10) || "",
      maxTemp: parseInt(schedule.maxTemp, 10) || ""
    });
  }
  addSchedule() {
    this.store.dispatch({
      type: ToleranceActions.ADD_SCHEDULE,
      payload: this.addScheduleForm.value
    });
    this.toleranceService
      .addSchedule(this.addScheduleForm.value, this.tolerance._id)
      .subscribe(
        res => {
          this.schedulesForm.reset();
          this.toast.setMessage("Schedule added successfully.", "success");
          this.store.dispatch({
            type: ToleranceActions.SELECT_TOLERANCE,
            payload: this.tolerance
          });
        },
        error => console.log(error)
      );
  }
  onRemove(index) {
    const control = <FormArray>this.schedulesForm.controls["schedulesControls"];
    control.removeAt(index);
    this.store.dispatch({
      type: ToleranceActions.REMOVE_SCHEDULE,
      payload: index
    });
    this.toleranceService
      .removeSchedule(this.tolerance.schedule[index], this.tolerance._id)
      .subscribe(
        res => {
          this.toast.setMessage("Schedule deleted successfully.", "danger");
          this.store.dispatch({
            type: ToleranceActions.SELECT_TOLERANCE,
            payload: this.tolerance
          });
        },
        error => console.log(error)
      );
  }
  getSchedules(sform) {
    return sform.get("schedulesControls").controls;
  }
}
