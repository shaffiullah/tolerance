import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToleranceDetailComponent } from './tolerance-detail.component';

describe('ToleranceDetailComponent', () => {
  let component: ToleranceDetailComponent;
  let fixture: ComponentFixture<ToleranceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToleranceDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToleranceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
