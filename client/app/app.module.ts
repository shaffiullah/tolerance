import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import { HttpClient, HttpClientModule } from "@angular/common/http";

import { RoutingModule } from "./routing.module";
import { SharedModule } from "./shared/shared.module";
import { AppComponent } from "./app.component";
import { ToleranceComponent } from "./tolerance/tolerance.component";
import { ToleranceDetailComponent } from "./tolerance-detail/tolerance-detail.component";
import { ToleranceChartComponent } from "./tolerance-chart/tolerance-chart.component";
import { ToleranceService } from "./services/tolerance.service";
import { ChartsModule } from "ng2-charts/ng2-charts";

import {ToleranceReducer} from "./store/tolerance/tolerance.reducers";
// import { TodoEffects } from './store/tolerance/';
@NgModule({
  declarations: [
    AppComponent,
    ToleranceComponent,
    ToleranceDetailComponent,
    ToleranceChartComponent
  ],
  imports: [
    RoutingModule,
    SharedModule,
    ChartsModule,
    NgbModule.forRoot(),
    StoreModule.forRoot({ tolerance: ToleranceReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    })
  ],
  providers: [ ToleranceService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
