export default class Tolerance {
  _id?: number;
  name?: string;
  schedule?: any[];
  lineChartData?: Array<any>;
  lineChartLabels?: Array<any>;
  constructor() {
    this.name = "";
    this.schedule = [];
    this.lineChartData = [
      { data: [], label: "Min Temp" },
      { data: [], label: "Max Temp" }
    ];
    this.lineChartLabels = [];
  }

  static generateMockTolerance(): Tolerance {
    return {
      name: "Tolerance_one",
      schedule: [],
      lineChartData: [
        { data: [], label: "Min Temp" },
        { data: [], label: "Max Temp" }
      ],
      lineChartLabels: []
    };
  }
}
