import { TestBed, inject } from '@angular/core/testing';

import { ToleranceService } from './tolerance.service';

describe('ToleranceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToleranceService]
    });
  });

  it('should be created', inject([ToleranceService], (service: ToleranceService) => {
    expect(service).toBeTruthy();
  }));
});
