import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ToleranceService {

  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  getTolerances(): Observable<any> {
    return this.http.get('/api/tolerances').map(res => res.json());
  }

  countTolerances(): Observable<any> {
    return this.http.get('/api/tolerances/count').map(res => res.json());
  }

  addTolerance(tolerance): Observable<any> {
    return this.http.post('/api/tolerance', JSON.stringify(tolerance), this.options);
  }

  getTolerance(tolerance): Observable<any> {
    return this.http.get(`/api/tolerance/${tolerance._id}`).map(res => res.json());
  }

  editTolerance(tolerance): Observable<any> {
    return this.http.put(`/api/tolerance/${tolerance._id}`, JSON.stringify(tolerance), this.options);
  }

  deleteTolerance(tolerance): Observable<any> {
    return this.http.delete(`/api/tolerance/${tolerance._id}`, this.options);
  }

  addSchedule(schedule, toleranceID) {
    return this.http.put(`/api/tolerance/addschedule/${toleranceID}`, JSON.stringify(schedule), this.options);
  }
  removeSchedule(schedule, toleranceID) {
    return this.http.put(`/api/tolerance/removeschedule/${toleranceID}`, JSON.stringify(schedule), this.options);
  }
}
