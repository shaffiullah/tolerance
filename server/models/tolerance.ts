import * as mongoose from 'mongoose';

export interface Schedule {
  startTime?: Date;
  endTime?: Date;
  minTemp?: Number;
  maxTemp?: Number;
}

const toleranceSchema = new mongoose.Schema({
  name: String,
  schedule: <Schedule>[]
});

const Tolerance = mongoose.model('Tolerance', toleranceSchema);

export default Tolerance;
