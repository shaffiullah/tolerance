import * as express from 'express';


import ToleranceCtrl from './controllers/tolerance';
import Tolerance from './models/tolerance';

export default function setRoutes(app) {

  const router = express.Router();

  const toleranceCtrl = new ToleranceCtrl();


 // tolerances
 router.route('/tolerances').get(toleranceCtrl.getAll);
 router.route('/tolerances/count').get(toleranceCtrl.count);
 router.route('/tolerance').post(toleranceCtrl.insert);
 router.route('/tolerance/:id').get(toleranceCtrl.get);
 router.route('/tolerance/:id').put(toleranceCtrl.update);
 router.route('/tolerance/addschedule/:id').put(toleranceCtrl.addSchedule);
 router.route('/tolerance/removeschedule/:id').put(toleranceCtrl.removeSchedule);
 router.route('/tolerance/:id').delete(toleranceCtrl.delete);


  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}
